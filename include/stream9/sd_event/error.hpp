#ifndef STREAM9_SD_EVENT_ERROR_HPP
#define STREAM9_SD_EVENT_ERROR_HPP

#include <stream9/linux/error.hpp>

#include <source_location>
#include <system_error>

namespace stream9::sd_event {

class error : public stream9::linux::error
{
    using base_t = stream9::linux::error;

public:
    using base_t::base_t;
    using base_t::operator=;

    error(std::string what,
          int negative_errno,
          std::source_location where = std::source_location::current()) noexcept;

    error(std::string what,
          int negative_errno,
          std::string context,
          std::source_location where = std::source_location::current()) noexcept;

public:
    enum class event_errc {
        invalid_extra_data = 1,
    };

    static std::error_category const& category();

    friend std::error_code make_error_code(event_errc);
};

inline error::
error(std::string what,
      int negative_errno,
      std::source_location where) noexcept
    : base_t { std::move(what), -negative_errno, std::move(where) }
{}

inline error::
error(std::string what,
      int negative_errno,
      std::string context,
      std::source_location where) noexcept
    : base_t { std::move(what), -negative_errno,
               std::move(context), std::move(where) }
{}

inline std::error_code
make_error_code(error::event_errc const ec)
{
    return { static_cast<int>(ec), error::category() };
}

} // namespace stream9::sd_event

namespace std {

template<>
struct is_error_code_enum<stream9::sd_event::error::event_errc>
    : true_type {};

template<>
struct is_error_condition_enum<stream9::sd_event::error::event_errc>
    : true_type {};

} // namespace std

#endif // STREAM9_SD_EVENT_ERROR_HPP
