#ifndef STREAM9_SD_EVENT_NAMESPACE_HPP
#define STREAM9_SD_EVENT_NAMESPACE_HPP

namespace stream9::strings {}

namespace stream9::sd_event {

namespace str { using namespace stream9::strings; }

} // namespace stream9::sd_event

#endif // STREAM9_SD_EVENT_NAMESPACE_HPP
