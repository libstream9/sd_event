#ifndef STREAM9_SD_EVENT_EVENT_SOURCE_HPP
#define STREAM9_SD_EVENT_EVENT_SOURCE_HPP

#include "namespace.hpp"

#include <functional>
#include <memory>
#include <utility>

#include <systemd/sd-event.h>

#include <stream9/strings.hpp>

namespace stream9::sd_event {

struct event_source_data;

class [[nodiscard]] event_source
{
public:
    using prepare_handler = std::function<int(event_source)>;
    using destroy_handler = std::function<void(void*)>;

public:
    event_source(::sd_event_source&);
    event_source(::sd_event_source&, std::unique_ptr<event_source_data>);

    ~event_source() noexcept;

    event_source(event_source const&) noexcept;
    event_source& operator=(event_source const&) noexcept;

    // query
    class event event() const noexcept;
    void* userdata() const noexcept;
    str::cstring_view description() const;
    bool pending() const;
    int64_t priority() const;
    int enabled() const;
    bool floating() const;
    bool exit_on_failure() const;

    std::pair<uint64_t/*interval_usec*/, unsigned/*burst*/>
        ratelimit() const;
    bool is_ratelimited() const;

    destroy_handler const& destroy_callback() const;

    // modifier
    void* set_userdata(void* userdata) const noexcept;
    void set_description(str::cstring_view);
    void set_priority(int64_t priority);
    void set_enabled(int enabled);
    void set_floating(bool b);
    void set_exit_on_failure(bool b);
    void set_ratelimit(uint64_t interval_usec, unsigned burst);

    void set_prepare(prepare_handler);
    void set_destroy_callback(destroy_handler);

    // command
    void send_child_signal(int sig, siginfo_t const*si, unsigned flags);

    void swap(event_source&) noexcept;

protected:
    ::sd_event_source* m_handle;
};

class [[nodiscard]] time_event_source : public event_source
{
public:
    using duration = std::chrono::microseconds;

public:
    time_event_source(::sd_event_source&);
    time_event_source(::sd_event_source&, std::unique_ptr<event_source_data>);

    time_event_source(time_event_source const&) noexcept;
    time_event_source& operator=(time_event_source const&) noexcept;

    // query
    duration time() const;
    duration time_accuracy() const;
    clockid_t time_clock() const;

    // modifier
    void set_time(duration);
    void set_time_relative(duration);
    void set_time_accuracy(duration);
};

class [[nodiscard]] io_event_source : public event_source
{
public:
    io_event_source(::sd_event_source&);
    io_event_source(::sd_event_source&, std::unique_ptr<event_source_data>);

    io_event_source(io_event_source const&) noexcept;
    io_event_source& operator=(io_event_source const&) noexcept;

    // query
    int io_fd() const;
    bool io_fd_own() const;
    uint32_t io_events() const;
    uint32_t io_revents() const;

    // modifier
    void set_io_fd(int fd);
    void set_io_fd_own(bool b);
    void set_io_events(uint32_t events);
};

class [[nodiscard]] signal_event_source : public event_source
{
public:
    signal_event_source(::sd_event_source&);
    signal_event_source(::sd_event_source&, std::unique_ptr<event_source_data>);

    signal_event_source(signal_event_source const&) noexcept;
    signal_event_source& operator=(signal_event_source const&) noexcept;

    // query
    int signal() const;
};

class [[nodiscard]] child_event_source : public event_source
{
public:
    child_event_source(::sd_event_source&);
    child_event_source(::sd_event_source&, std::unique_ptr<event_source_data>);

    child_event_source(child_event_source const&) noexcept;
    child_event_source& operator=(child_event_source const&) noexcept;

    // query
    pid_t child_pid() const;
    int child_pidfd() const;
    bool child_pidfd_own() const;
    bool child_process_own() const;

    // modifier
    void set_child_pidfd_own(bool own);
    void set_child_process_own(bool own);
};

class [[nodiscard]] inotify_event_source : public event_source
{
public:
    inotify_event_source(::sd_event_source&);
    inotify_event_source(::sd_event_source&, std::unique_ptr<event_source_data>);

    inotify_event_source(inotify_event_source const&) noexcept;
    inotify_event_source& operator=(inotify_event_source const&) noexcept;

    // query
    uint32_t inotify_mask() const;
};

} // namespace stream9::sd_event

#if 0 //TODO
sd_event_source* sd_event_source_disable_unref(sd_event_source *s);
#endif

#endif // STREAM9_SD_EVENT_EVENT_SOURCE_HPP
