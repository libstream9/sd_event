#ifndef STREAM9_SD_EVENT_EVENT_HPP
#define STREAM9_SD_EVENT_EVENT_HPP

#include "event_source.hpp"

#include <chrono>
#include <functional>

#include <systemd/sd-event.h>

#include <stream9/strings.hpp>

namespace stream9::sd_event {

class event_source;

class [[nodiscard]] event
{
public:
    using system_time = std::chrono::time_point<std::chrono::system_clock>;
    using steady_time = std::chrono::time_point<std::chrono::steady_clock>;
    using duration = std::chrono::microseconds;

    using event_handler = std::function<int(event_source)>;
    using time_handler = std::function<int(time_event_source, uint64_t/*usec*/)>;
    using io_handler
        = std::function<int(io_event_source, int/*fd*/, uint32_t/*revents*/)>;
    using signal_handler
        = std::function<int(signal_event_source, struct signalfd_siginfo const&)>;
    using child_handler
        = std::function<int(child_event_source, siginfo_t const&)>;
    using inotify_handler
        = std::function<int(inotify_event_source, struct inotify_event const&)>;

public:
    event();
    event(::sd_event*) noexcept;

    ~event() noexcept;

    event(event const&) noexcept;
    event& operator=(event const&) noexcept;

    // query
    pid_t tid() const;
    int fd() const;
    int state() const;
    int exit_code() const;
    bool watchdog_enabled() const;
    uint64_t iteration() const;
    uint64_t now(clockid_t clock) const;

    // modifier
    event_source add_defer(event_handler);

    time_event_source
            add_time(clockid_t, duration usec, duration accuracy, time_handler);

    time_event_source add_time(system_time, time_handler);
    time_event_source add_time(steady_time, time_handler);

    time_event_source add_time_relative(duration, time_handler);

    io_event_source add_io(int fd, uint32_t events, io_handler);

    signal_event_source add_signal(int sig, signal_handler);

    child_event_source add_child(pid_t pid, int options, child_handler);

    child_event_source add_child_pidfd(int pidfd, int options, child_handler);

    inotify_event_source
        add_inotify(str::cstring_view path, uint32_t mask, inotify_handler);

    event_source add_post(event_handler);

    event_source add_exit(event_handler);

    void set_watchdog(bool b);

    // command
    int prepare();
    int dispatch();
    int wait(uint64_t usec);

    void exit(int code);
    void run(uint64_t usec);
    void loop();

    void swap(event&) noexcept;

    // conversion
    operator ::sd_event* () const { return m_handle; }

private:
    ::sd_event* m_handle; // non-null
};

event default_event();

} // namespace stream9::sd_event

#endif // STREAM9_SD_EVENT_EVENT_HPP
