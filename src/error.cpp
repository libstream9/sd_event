#include <stream9/sd_event/error.hpp>

#include <system_error>

namespace stream9::sd_event {

std::error_category const& error::
category()
{
    static struct impl : public std::error_category
    {
        char const* name() const noexcept
        {
            return "stream9::sd_event::error";
        }

        std::string message(int const condition) const
        {
            using enum event_errc;

            switch (static_cast<event_errc>(condition)) {
                case invalid_extra_data:
                    return "invalid extra data";
            }

            return "unknown error";
        }
    } instance;

    return instance;
}

} // namespace stream9::sd_event
