#include <stream9/sd_event/event.hpp>

#include "event_source_p.hpp"

#include <system_error>
#include <utility>

#include <stream9/sd_event/error.hpp>

#include <stream9/json.hpp>
#include <stream9/strings.hpp>

namespace stream9::sd_event {

static int
on_event(::sd_event_source* const s, void* const userdata)
{
    assert(s);
    assert(userdata);

    auto& data = *static_cast<event_source_data*>(userdata);

    auto& handler = std::get<event_handler>(data.handler);

    return handler(*::sd_event_source_ref(s));
}

static int
on_time(::sd_event_source* const s, uint64_t const usec, void* const userdata)
{
    assert(s);
    assert(userdata);

    auto& data = *static_cast<event_source_data*>(userdata);

    auto& handler = std::get<time_handler>(data.handler);

    return handler(*::sd_event_source_ref(s), usec);
}

static int
on_io(::sd_event_source* const s, int const fd, uint32_t const revents,
      void* const userdata)
{
    assert(s);
    assert(userdata);

    auto& data = *static_cast<event_source_data*>(userdata);

    auto& handler = std::get<io_handler>(data.handler);

    return handler(*::sd_event_source_ref(s), fd, revents);
}

static int
on_signal(::sd_event_source* const s, struct signalfd_siginfo const* const si,
          void* const userdata)
{
    assert(s);
    assert(si);
    assert(userdata);

    auto& data = *static_cast<event_source_data*>(userdata);

    auto& handler = std::get<signal_handler>(data.handler);

    return handler(*::sd_event_source_ref(s), *si);
}

static int
on_child(::sd_event_source* const s, siginfo_t const* const si,
         void* const userdata)
{
    assert(s);
    assert(si);
    assert(userdata);

    auto& data = *static_cast<event_source_data*>(userdata);

    auto& handler = std::get<child_handler>(data.handler);

    return handler(*::sd_event_source_ref(s), *si);
}

static int
on_inotify(::sd_event_source* const s, struct inotify_event const* const e,
           void* const userdata)
{
    assert(s);
    assert(e);
    assert(userdata);

    auto& data = *static_cast<event_source_data*>(userdata);

    auto& handler = std::get<inotify_handler>(data.handler);

    return handler(*::sd_event_source_ref(s), *e);
}

/*
 * class event
 */
event::
event()
{
    auto const rv = ::sd_event_new(&m_handle);
    if (rv < 0) {
        throw error { "sd_event_new", rv, };
    }
}

event::
event(::sd_event* const h) noexcept
    : m_handle { h }
{}

event::
~event() noexcept
{
    ::sd_event_unref(m_handle);
}

event::
event(event const& other) noexcept
    : m_handle { ::sd_event_ref(other.m_handle) }
{}

event& event::
operator=(event const& other) noexcept
{
    event tmp { other };
    swap(tmp);

    return *this;
}

pid_t event::
tid() const
{
    pid_t result;

    auto const rv = ::sd_event_get_tid(m_handle, &result);
    if (rv < 0) {
        throw error { "sd_event_get_tid", rv };
    }

    return result;
}

int event::
fd() const
{
    auto const rv = ::sd_event_get_fd(m_handle);
    if (rv < 0) {
        throw error { "sd_event_get_fd", rv };
    }

    return rv;
}

int event::
state() const
{
    auto const rv = ::sd_event_get_state(m_handle);
    if (rv < 0) {
        throw error { "sd_event_get_state", rv };
    }

    return rv;
}

int event::
exit_code() const
{
    int result = 0;
    auto const rv = ::sd_event_get_exit_code(m_handle, &result);
    if (rv < 0) {
        throw error { "sd_event_get_exit_code", rv };
    }

    return result;
}

bool event::
watchdog_enabled() const
{
    auto const rv = ::sd_event_get_watchdog(m_handle);
    if (rv < 0) {
        throw error { "sd_event_get_watchdog", rv };
    }

    return rv;
}

uint64_t event::
iteration() const
{
    uint64_t result = 0;
    auto const rv = ::sd_event_get_iteration(m_handle, &result);
    if (rv < 0) {
        throw error { "sd_event_get_iteration", rv };
    }

    return result;
}

uint64_t event::
now(clockid_t const clock) const
{
    uint64_t result = 0;
    auto const rv = ::sd_event_now(m_handle, clock, &result);
    if (rv < 0) {
        throw error { "sd_event_now", rv };
    }

    return result;
}

event_source event::
add_defer(event_handler h)
{
    try {
        ::sd_event_source* result = nullptr;

        auto data = std::make_unique<event_source_data>();
        data->handler = std::move(h);

        auto rc =
            ::sd_event_add_defer(m_handle, &result, on_event, data.get());
        if (rc < 0) {
            throw error { "sd_event_add_defer", rc };
        }

        assert(result);
        return { *result, std::move(data) };
    }
    catch (...) {
        std::throw_with_nested(error()
            .why(error::errc::internal_error)
        );
    }
}

time_event_source event::
add_time(clockid_t const clock, duration const usec, duration const accuracy,
         time_handler h)
{
    try {
        namespace C = std::chrono;

        ::sd_event_source* s {};

        auto data = std::make_unique<event_source_data>();
        data->handler = std::move(h);

        auto rc = ::sd_event_add_time(
            m_handle, &s,
            clock,
            static_cast<uint64_t>(usec.count()),
            static_cast<uint64_t>(accuracy.count()),
            on_time, data.get()
        );
        if (rc < 0) {
            throw error {
                "sd_event_add_time", rc, str::to_string(json::object {
                    { "clock", clock },
                    { "usec", str::to_string(usec) },
                    { "accuracy", str::to_string(accuracy) },
                })
            };
        }

        assert(s);
        return { *s, std::move(data) };
    }
    catch (...) {
        std::throw_with_nested(error()
            .why(error::errc::internal_error)
        );
    }
}

time_event_source event::
add_time(system_time const tp, time_handler h)
{
    try {
        namespace C = std::chrono;

        ::sd_event_source* s {};

        auto data = std::make_unique<event_source_data>();
        data->handler = std::move(h);

        auto const usec = static_cast<uint64_t>(
            C::duration_cast<C::microseconds>(tp.time_since_epoch()).count() );

        auto rc = ::sd_event_add_time(
                    m_handle, &s, CLOCK_REALTIME, usec, 0, on_time, data.get());
        if (rc < 0) {
            throw error {
                "sd_event_add_time", rc, str::to_string(json::object {
                    { "usec", usec },
                })
            };
        }

        assert(s);
        return { *s, std::move(data) };
    }
    catch (...) {
        std::throw_with_nested(error()
            .why(error::errc::internal_error)
        );
    }
}

time_event_source event::
add_time(steady_time const tp, time_handler h)
{
    try {
        namespace C = std::chrono;

        ::sd_event_source* s {};

        auto data = std::make_unique<event_source_data>();
        data->handler = std::move(h);

        auto const usec = static_cast<uint64_t>(
            C::duration_cast<C::microseconds>(tp.time_since_epoch()).count() );

        auto rc = ::sd_event_add_time(
                   m_handle, &s, CLOCK_MONOTONIC, usec, 0, on_time, data.get());
        if (rc < 0) {
            throw error {
                "sd_event_add_time", rc, str::to_string(json::object {
                    { "usec", usec },
                })
            };
        }

        assert(s);
        return { *s, std::move(data) };
    }
    catch (...) {
        std::throw_with_nested(error()
            .why(error::errc::internal_error)
        );
    }
}

time_event_source event::
add_time_relative(duration const d, time_handler h)
{
    try {
        namespace C = std::chrono;

        ::sd_event_source* s {};

        auto data = std::make_unique<event_source_data>();
        data->handler = std::move(h);

        auto const usec = static_cast<uint64_t>(d.count());

        auto rc = ::sd_event_add_time_relative(
                   m_handle, &s, CLOCK_MONOTONIC, usec, 0, on_time, data.get());
        if (rc < 0) {
            throw error {
                "sd_event_add_time_relative", rc, str::to_string(json::object {
                    { "usec", usec },
                })
            };
        }

        assert(s);
        return { *s, std::move(data) };
    }
    catch (...) {
        std::throw_with_nested(error()
            .why(error::errc::internal_error)
        );
    }
}

io_event_source event::
add_io(int const fd, uint32_t const events, io_handler h)
{
    try {
        ::sd_event_source* s {};

        auto data = std::make_unique<event_source_data>();
        data->handler = std::move(h);

        auto rc =
            ::sd_event_add_io(m_handle, &s, fd, events, on_io, data.get());
        if (rc < 0) {
            throw error {
                "sd_event_add_io", rc, str::to_string(json::object {
                    { "fd", fd },
                    { "events", events },
                })
            };
        }

        assert(s);
        return { *s, std::move(data) };
    }
    catch (...) {
        std::throw_with_nested(error()
            .why(error::errc::internal_error)
        );
    }

}

signal_event_source event::
add_signal(int const sig, signal_handler h)
{
    try {
        ::sd_event_source* s {};

        auto data = std::make_unique<event_source_data>();
        data->handler = std::move(h);

        auto rc =
                ::sd_event_add_signal(m_handle, &s, sig, on_signal, data.get());
        if (rc < 0) {
            throw error { "sd_event_add_signal", rc,
                str::to_string(json::object {
                    { "signal", sig }
                })
            };
        }

        assert(s);
        return { *s, std::move(data) };
    }
    catch (...) {
        std::throw_with_nested(error()
            .why(error::errc::internal_error)
        );
    }
}

child_event_source event::
add_child(pid_t const pid, int const options, child_handler h)
{
    try {
        ::sd_event_source* s {};

        auto data = std::make_unique<event_source_data>();
        data->handler = std::move(h);

        auto rc = ::sd_event_add_child(
                              m_handle, &s, pid, options, on_child, data.get());
        if (rc < 0) {
            throw error { "sd_event_add_child", rc,
                str::to_string(json::object {
                    { "pid", pid },
                    { "options", options },
                }) };
        }

        assert(s);
        return { *s, std::move(data) };
    }
    catch (...) {
        std::throw_with_nested(error()
            .why(error::errc::internal_error)
        );
    }
}

child_event_source event::
add_child_pidfd(int const pidfd, int const options, child_handler h)
{
    try {
        ::sd_event_source* s {};

        auto data = std::make_unique<event_source_data>();
        data->handler = std::move(h);

        auto rc = ::sd_event_add_child_pidfd(
                            m_handle, &s, pidfd, options, on_child, data.get());
        if (rc < 0) {
            throw error { "sd_event_add_child_pidfd", rc,
                str::to_string(json::object {
                    { "pidfd", pidfd },
                    { "options", options },
                })
            };
        }

        assert(s);
        return { *s, std::move(data) };
    }
    catch (...) {
        std::throw_with_nested(error()
            .why(error::errc::internal_error)
        );
    }

}

inotify_event_source event::
add_inotify(str::cstring_view const path, uint32_t const mask, inotify_handler h)
{
    try {
        ::sd_event_source* s {};

        auto data = std::make_unique<event_source_data>();
        data->handler = std::move(h);

        auto rc = ::sd_event_add_inotify(
                              m_handle, &s, path, mask, on_inotify, data.get());
        if (rc < 0) {
            throw error { "sd_event_add_inotify", rc,
                str::to_string(json::object {
                    { "path", path },
                    { "mask", mask },
                })
            };
        }

        assert(s);
        return { *s, std::move(data) };
    }
    catch (...) {
        std::throw_with_nested(error()
            .why(error::errc::internal_error)
        );
    }
}

event_source event::
add_post(event_handler h)
{
    try {
        ::sd_event_source* s {};

        auto data = std::make_unique<event_source_data>();
        data->handler = std::move(h);

        auto rc = ::sd_event_add_post(m_handle, &s, on_event, data.get());
        if (rc < 0) {
            throw error { "sd_event_add_post", rc };
        }

        assert(s);
        return { *s, std::move(data) };
    }
    catch (...) {
        std::throw_with_nested(error()
            .why(error::errc::internal_error)
        );
    }
}

event_source event::
add_exit(event_handler h)
{
    try {
        ::sd_event_source* s {};

        auto data = std::make_unique<event_source_data>();
        data->handler = std::move(h);

        auto rc = ::sd_event_add_exit(m_handle, &s, on_event, data.get());
        if (rc < 0) {
            throw error { "sd_event_add_exit", rc };
        }

        assert(s);
        return { *s, std::move(data) };
    }
    catch (...) {
        std::throw_with_nested(error()
            .why(error::errc::internal_error)
        );
    }
}

void event::
set_watchdog(bool const b)
{
    auto const rc = ::sd_event_set_watchdog(m_handle, b);
    if (rc < 0) {
        throw error { "sd_event_set_watchdog", rc };
    }
}

int event::
prepare()
{
    auto const rc = ::sd_event_prepare(m_handle);
    if (rc < 0) {
        throw error { "sd_event_prepare", rc };
    }

    return rc;
}

int event::
dispatch()
{
    auto const rc = ::sd_event_dispatch(m_handle);
    if (rc < 0) {
        throw error { "sd_event_dispatch", rc };
    }

    return rc;
}

int event::
wait(uint64_t const usec)
{
    auto const rc = ::sd_event_wait(m_handle, usec);
    if (rc < 0) {
        throw error { "sd_event_wait", rc };
    }

    return rc;
}

void event::
exit(int const code)
{
    auto const rv = ::sd_event_exit(m_handle, code);
    if (rv < 0) {
        throw error { "sd_event_exit", rv };
    }
}

void event::
run(uint64_t const usec)
{
    auto const rv = ::sd_event_run(m_handle, usec);
    if (rv < 0) {
        throw error { "sd_event_run", rv };
    }
}

void event::
loop()
{
    auto const rv = ::sd_event_loop(m_handle);
    if (rv < 0) {
        throw error { "sd_event_loop", rv };
    }
}

void event::
swap(event& other) noexcept
{
    using std::swap;
    swap(m_handle, other.m_handle);
}

event
default_event()
{
    ::sd_event* result = nullptr;

    auto const rv = ::sd_event_default(&result);
    if (rv < 0) {
        throw error { "sd_event_default", rv };
    }

    return result;
}

} // namespace stream9::sd_event
