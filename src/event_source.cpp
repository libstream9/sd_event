#include <stream9/sd_event/event_source.hpp>

#include "event_source_p.hpp"

#include <stream9/sd_event/event.hpp>
#include <stream9/sd_event/error.hpp>

#include <utility>

#include <stream9/json.hpp>

namespace stream9::sd_event {

static event_source_data*
opt_get_data(::sd_event_source* const h)
{
    return static_cast<event_source_data*>(::sd_event_source_get_userdata(h));
}

static event_source_data&
get_data(::sd_event_source* const h)
{
     auto const result = opt_get_data(h);
     assert(result);

     return *result;
}

static int
on_prepare(::sd_event_source* const s, void* const userdata)
{
    assert(s);
    assert(userdata);

    auto& data = *static_cast<event_source_data*>(userdata);

    auto& handler = data.prepare;

    return handler(*s);
}

static void
on_source_destroyed(void* userdata)
{
    if (userdata == nullptr) return;

    auto* data = static_cast<event_source_data*>(userdata);
    if (data->destroy) {
        data->destroy(userdata);
    }

    delete data;
}

/*
 * class event_source
 */
event_source::
event_source(::sd_event_source& h)
    : m_handle { &h }
{
    auto const data = opt_get_data(m_handle);
    if (!data || !data->validate()) {
        throw error().why(error::event_errc::invalid_extra_data);
    }
}

event_source::
event_source(::sd_event_source& h, std::unique_ptr<event_source_data> data)
    : m_handle { &h }
{
    auto const rc =
          ::sd_event_source_set_destroy_callback(m_handle, on_source_destroyed);
    if (rc < 0) {
        ::sd_event_source_unref(m_handle);
        throw error { "sd_event_source_set_destroy_callback", rc };
    }

    data.release();
}

event_source::
~event_source() noexcept
{
    ::sd_event_source_unref(m_handle);
}

event_source::
event_source(event_source const& other) noexcept
    : event_source { *::sd_event_source_ref(other.m_handle) }
{}

event_source& event_source::
operator=(event_source const& other) noexcept
{
    event_source tmp { other };
    swap(tmp);
    return *this;
}

class event event_source::
event() const noexcept
{
    assert(m_handle);
    return ::sd_event_ref(::sd_event_source_get_event(m_handle));
}

void* event_source::
userdata() const noexcept
{
    assert(m_handle);
    return get_data(m_handle).userdata;
}

str::cstring_view event_source::
description() const
{
    char const* result = nullptr;

    auto const rc = ::sd_event_source_get_description(m_handle, &result);
    if (rc < 0) {
        throw error { "sd_event_source_get_description", rc };
    }

    return result;
}

bool event_source::
pending() const
{
    auto const rc = ::sd_event_source_get_pending(m_handle);
    if (rc > 0) {
        return true;
    }
    else if (rc == 0) {
        return false;
    }
    else {
        throw error { "sd_event_source_get_pending", rc };
    }
}

int64_t event_source::
priority() const
{
    int64_t result {};

    auto const rc = ::sd_event_source_get_priority(m_handle, &result);
    if (rc < 0) {
        throw error { "sd_event_source_get_priority", rc };
    }

    return result;
}

int event_source::
enabled() const
{
    int result {};

    auto const rc = ::sd_event_source_get_enabled(m_handle, &result);
    if (rc < 0) {
        throw error { "sd_event_source_get_enabled", rc };
    }

    return result;
}

bool event_source::
floating() const
{
    auto const rc = ::sd_event_source_get_floating(m_handle);
    if (rc < 0) {
        throw error { "sd_event_source_get_floating", rc };
    }

    return rc;
}

bool event_source::
exit_on_failure() const
{
    auto const rc = ::sd_event_source_get_exit_on_failure(m_handle);
    if (rc < 0) {
        throw error { "sd_event_source_get_exit_on_failure", rc };
    }

    return rc;
}

std::pair<uint64_t/*interval_usec*/, unsigned/*burst*/> event_source::
ratelimit() const
{
    uint64_t interval_usec {};
    unsigned burst {};

    auto const rc =
        ::sd_event_source_get_ratelimit(m_handle, &interval_usec, &burst);
    if (rc < 0) {
        throw error { "sd_event_source_get_ratelimit", rc };
    }

    return { interval_usec, burst };
}

bool event_source::
is_ratelimited() const
{
    auto const rc = ::sd_event_source_is_ratelimited(m_handle);
    if (rc < 0) {
        throw error { "sd_event_source_is_ratelimited", rc };
    }

    return rc;
}

event_source::destroy_handler const& event_source::
destroy_callback() const
{
    return get_data(m_handle).destroy;
}

void* event_source::
set_userdata(void* const userdata) const noexcept
{
    assert(m_handle);
    return std::exchange(get_data(m_handle).userdata, userdata);

}

void event_source::
set_description(str::cstring_view const s)
{
    auto const rc = ::sd_event_source_set_description(m_handle, s);
    if (rc < 0) {
        throw error {
            "sd_event_source_set_description", rc,
            str::to_string(json::object {
                { "description", s }
            })
        };
    }
}

void event_source::
set_priority(int64_t const priority)
{
    auto const rc = ::sd_event_source_set_priority(m_handle, priority);
    if (rc < 0) {
        throw error {
            "sd_event_source_set_priority", rc,
            str::to_string(json::object {
                { "priority", priority }
            })
        };
    }
}

void event_source::
set_enabled(int const enabled)
{
    auto const rc = ::sd_event_source_set_enabled(m_handle, enabled);
    if (rc < 0) {
        throw error { "sd_event_source_set_enabled", rc };
    }
}

void event_source::
set_floating(bool const b)
{
    auto const rc = ::sd_event_source_set_floating(m_handle, b);
    if (rc < 0) {
        throw error {
            "sd_event_source_set_floating", rc,
            str::to_string(json::object {
                { "b", b }
            })
        };
    }
}

void event_source::
set_exit_on_failure(bool const b)
{
    auto const rc = ::sd_event_source_set_exit_on_failure(m_handle, b);
    if (rc < 0) {
        throw error {
            "sd_event_source_set_exit_on_failure", rc,
            str::to_string(json::object {
                { "b", b }
            })
        };
    }
}

void event_source::
set_ratelimit(uint64_t const interval_usec, unsigned const burst)
{
    auto const rc =
        ::sd_event_source_set_ratelimit(m_handle, interval_usec, burst);
    if (rc < 0) {
        throw error {
            "sd_event_source_set_ratelimit", rc,
            str::to_string(json::object {
                { "interval_usec", interval_usec },
                { "burst", burst },
            })
        };
    }
}

void event_source::
set_prepare(prepare_handler h)
{
    auto const rc = ::sd_event_source_set_prepare(m_handle, on_prepare);
    if (rc < 0) {
        throw error { "sd_event_source_set_prepare", rc, };
    }

    get_data(m_handle).prepare = std::move(h);
}

void event_source::
set_destroy_callback(destroy_handler h)
{
    get_data(m_handle).destroy = std::move(h);
}

void event_source::
send_child_signal(int const sig,
                  siginfo_t const* const si, unsigned const flags)
{
    auto const rc = ::sd_event_source_send_child_signal(m_handle, sig, si, flags);
    if (rc < 0) {
        throw error {
            "sd_event_source_send_child_signal", rc,
            str::to_string(json::object {
                { "sig", sig },
                { "si", str::to_string(si) },
                { "flags", flags },
            })
        };
    }
}

void event_source::
swap(event_source& other) noexcept
{
    using std::swap;
    swap(m_handle, other.m_handle);
}

/*
 * time_event_source
 */
time_event_source::
time_event_source(::sd_event_source& h)
    : event_source { h }
{}

time_event_source::
time_event_source(::sd_event_source& h, std::unique_ptr<event_source_data> d)
    : event_source { h, std::move(d) }
{}

time_event_source::
time_event_source(time_event_source const& other) noexcept
    : event_source { other }
{}

time_event_source& time_event_source::
operator=(time_event_source const& other) noexcept
{
    time_event_source tmp { other };
    swap(tmp);
    return *this;
}

time_event_source::duration time_event_source::
time() const
{
    uint64_t result {};

    auto const rc = ::sd_event_source_get_time(m_handle, &result);
    if (rc < 0) {
        throw error { "sd_event_source_get_time", rc };
    }

    return duration { result };
}

time_event_source::duration time_event_source::
time_accuracy() const
{
    uint64_t result {};

    auto const rc = ::sd_event_source_get_time_accuracy(m_handle, &result);
    if (rc < 0) {
        throw error { "sd_event_source_get_time_accuracy", rc };
    }

    return duration { result };
}

clockid_t time_event_source::
time_clock() const
{
    clockid_t result {};

    auto const rc = ::sd_event_source_get_time_clock(m_handle, &result);
    if (rc < 0) {
        throw error { "sd_event_source_get_time_clock", rc };
    }

    return result;
}

void time_event_source::
set_time(duration const usec)
{
    auto const rc = ::sd_event_source_set_time(
                               m_handle, static_cast<uint64_t>(usec.count()));
    if (rc < 0) {
        throw error {
            "sd_event_source_set_time", rc,
            str::to_string(json::object {
                { "usec", str::to_string(usec) }
            })
        };
    }
}

void time_event_source::
set_time_relative(duration const usec)
{
    auto const rc = ::sd_event_source_set_time_relative(
                               m_handle, static_cast<uint64_t>(usec.count()));
    if (rc < 0) {
        throw error {
            "sd_event_source_set_time_relative", rc,
            str::to_string(json::object {
                { "usec", str::to_string(usec) }
            })
        };
    }
}

void time_event_source::
set_time_accuracy(duration const usec)
{
    auto const rc = ::sd_event_source_set_time_accuracy(
                               m_handle, static_cast<uint64_t>(usec.count()));
    if (rc < 0) {
        throw error {
            "sd_event_source_set_time_accuracy", rc,
            str::to_string(json::object {
                { "usec", str::to_string(usec) }
            })
        };
    }
}

/*
 * io_event_source
 */
io_event_source::
io_event_source(::sd_event_source& h)
    : event_source { h }
{}

io_event_source::
io_event_source(::sd_event_source& h, std::unique_ptr<event_source_data> d)
    : event_source { h, std::move(d) }
{}

io_event_source::
io_event_source(io_event_source const& other) noexcept
    : event_source { other }
{}

io_event_source& io_event_source::
operator=(io_event_source const& other) noexcept
{
    io_event_source tmp { other };
    swap(tmp);
    return *this;
}

int io_event_source::
io_fd() const
{
    auto const rc = ::sd_event_source_get_io_fd(m_handle);
    if (rc < 0) {
        throw error { "sd_event_source_get_io_fd", rc };
    }

    return rc;
}

bool io_event_source::
io_fd_own() const
{
    auto const rc = ::sd_event_source_get_io_fd_own(m_handle);
    if (rc < 0) {
        throw error { "sd_event_source_get_io_fd_own", rc };
    }

    return rc;
}

uint32_t io_event_source::
io_events() const
{
    uint32_t result {};

    auto const rc = ::sd_event_source_get_io_events(m_handle, &result);
    if (rc < 0) {
        throw error { "sd_event_source_get_io_events", rc };
    }

    return result;
}

uint32_t io_event_source::
io_revents() const
{
    uint32_t result {};

    auto const rc = ::sd_event_source_get_io_revents(m_handle, &result);
    if (rc < 0) {
        throw error { "sd_event_source_get_io_revents", rc };
    }

    return result;
}

void io_event_source::
set_io_fd(int const fd)
{
    auto const rc = ::sd_event_source_set_io_fd(m_handle, fd);
    if (rc < 0) {
        throw error {
            "sd_event_source_set_io_fd", rc,
            str::to_string(json::object {
                { "fd", fd }
            })
        };
    }
}

void io_event_source::
set_io_fd_own(bool const b)
{
    auto const rc = ::sd_event_source_set_io_fd_own(m_handle, b);
    if (rc < 0) {
        throw error {
            "sd_event_source_set_io_fd_own", rc,
            str::to_string(json::object {
                { "b", b }
            })
        };
    }
}

void io_event_source::
set_io_events(uint32_t const events)
{
    auto const rc = ::sd_event_source_set_io_events(m_handle, events);
    if (rc < 0) {
        throw error {
            "sd_event_source_set_io_events", rc,
            str::to_string(json::object {
                { "event", events }
            })
        };
    }
}

/*
 * signal_event_source
 */
signal_event_source::
signal_event_source(::sd_event_source& h)
    : event_source { h }
{}

signal_event_source::
signal_event_source(::sd_event_source& h, std::unique_ptr<event_source_data> d)
    : event_source { h, std::move(d) }
{}

signal_event_source::
signal_event_source(signal_event_source const& other) noexcept
    : event_source { other }
{}

signal_event_source& signal_event_source::
operator=(signal_event_source const& other) noexcept
{
    signal_event_source tmp { other };
    swap(tmp);
    return *this;
}

int signal_event_source::
signal() const
{
    auto const rc = ::sd_event_source_get_signal(m_handle);
    if (rc < 0) {
        throw error { "sd_event_source_get_signal", rc };
    }

    return rc;
}

/*
 * child_event_source
 */
child_event_source::
child_event_source(::sd_event_source& h)
    : event_source { h }
{}

child_event_source::
child_event_source(::sd_event_source& h, std::unique_ptr<event_source_data> d)
    : event_source { h, std::move(d) }
{}

child_event_source::
child_event_source(child_event_source const& other) noexcept
    : event_source { other }
{}

child_event_source& child_event_source::
operator=(child_event_source const& other) noexcept
{
    child_event_source tmp { other };
    swap(tmp);
    return *this;
}

pid_t child_event_source::
child_pid() const
{
    pid_t result {};

    auto const rc = ::sd_event_source_get_child_pid(m_handle, &result);
    if (rc < 0) {
        throw error { "sd_event_source_get_child_pid", rc };
    }

    return result;
}

int child_event_source::
child_pidfd() const
{
    auto const rc = ::sd_event_source_get_child_pidfd(m_handle);
    if (rc < 0) {
        throw error { "sd_event_source_get_child_pidfd", rc };
    }

    return rc;
}

bool child_event_source::
child_pidfd_own() const
{
    auto const rc = ::sd_event_source_get_child_pidfd_own(m_handle);
    if (rc < 0) {
        throw error { "sd_event_source_get_child_pidfd_own", rc };
    }

    return rc;
}

bool child_event_source::
child_process_own() const
{
    auto const rc = ::sd_event_source_get_child_process_own(m_handle);
    if (rc < 0) {
        throw error { "sd_event_source_get_child_process_own", rc };
    }

    return rc;
}

void child_event_source::
set_child_pidfd_own(bool const own)
{
    auto const rc = ::sd_event_source_set_child_pidfd_own(m_handle, own);
    if (rc < 0) {
        throw error {
            "sd_event_source_set_child_pidfd_own", rc,
            str::to_string(json::object {
                { "own", own }
            })
        };
    }
}

void child_event_source::
set_child_process_own(bool const own)
{
    auto const rc = ::sd_event_source_set_child_process_own(m_handle, own);
    if (rc < 0) {
        throw error {
            "sd_event_source_set_child_process_own", rc,
            str::to_string(json::object {
                { "own", own }
            })
        };
    }
}

/*
 * inotify_event_source
 */
inotify_event_source::
inotify_event_source(::sd_event_source& h)
    : event_source { h }
{}

inotify_event_source::
inotify_event_source(::sd_event_source& h, std::unique_ptr<event_source_data> d)
    : event_source { h, std::move(d) }
{}

inotify_event_source::
inotify_event_source(inotify_event_source const& other) noexcept
    : event_source { other }
{}

inotify_event_source& inotify_event_source::
operator=(inotify_event_source const& other) noexcept
{
    inotify_event_source tmp { other };
    swap(tmp);
    return *this;
}

uint32_t inotify_event_source::
inotify_mask() const
{
    uint32_t result {};

    auto const rc = ::sd_event_source_get_inotify_mask(m_handle, &result);
    if (rc < 0) {
        throw error { "sd_event_source_get_inotify_mask", rc };
    }

    return result;
}

} // namespace stream9::sd_event
