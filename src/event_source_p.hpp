#ifndef SD_EVENT_EVENT_SOURCE_P_HPP
#define SD_EVENT_EVENT_SOURCE_P_HPP

#include <stream9/sd_event/event.hpp>

#include <functional>
#include <variant>

namespace stream9::sd_event {

using event_handler = event::event_handler;
using time_handler = event::time_handler;
using io_handler = event::io_handler;
using signal_handler = event::signal_handler;
using child_handler = event::child_handler;
using inotify_handler = event::inotify_handler;

struct event_source_data
{
    static constexpr uint32_t signature = 0x2E175EC4;

    uint32_t head = signature;

    std::variant<event_handler, time_handler,
                 io_handler, signal_handler,
                 child_handler, inotify_handler > handler {};

    event_handler prepare {};

    std::function<void(void*)> destroy {};

    void* userdata {};

    uint32_t tail = signature;

    bool validate() const
    {
        return head == signature && tail == signature;
    }
};

} // namespace stream9::sd_event

#endif // SD_EVENT_EVENT_SOURCE_P_HPP
